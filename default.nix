{ pkgs ? (builtins.getFlake "nixpkgs").legacyPackages.${builtins.currentSystem} }:
pkgs.poetry2nix.mkPoetryApplication {
  projectDir = ./.;
}
