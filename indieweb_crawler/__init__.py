import sys
import asyncio
import aiohttp
import mf2py
import whoosh

# monkey-patch mf2py
mf2py.mf_helpers.basestring = str

python_version = f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}"
__version__ = '0.1.0'
# fetch an initial page (feed?), crawl every document, add it to
# an index and then continue crawling through the MF2 markup
# recursively through a queue of URLs to be crawled
#
# Additionally make the work span multiple threads or be async
blocklisted_url_prefixes = ["https://twitter.com/", "https://facebook.com/"]
crawled_pages = set()

robotstxts = {}

def is_h_entry(mf2: dict) -> bool:
    "h-entry" in mf2["type"]

# TODO make worker-name task-local
async def crawl_page(worker: str, http: aiohttp.ClientSession, url: str):
    if url in crawled_pages:
        return set()
    for i in blocklisted_url_prefixes:
        if url.startswith(i):
            return set()
    print(f"[{worker}] Crawling {url}")
    crawled_pages.add(url)
    items = set()

    # XXX fetch robots.txt, parse it, cache it and check if we're allowed to fetch a URL
    print(f"Fetching {url}")
    async with http.get(url) as response:
        body = await response.text()
    print(f"Parsing {url}")
    parsed = mf2py.parse(doc=body, url=url)
        
    for item in parsed["items"]:
        if url in item["properties"].get("url", []) or url in item["properties"].get("uid", []):
            crawled_pages.update(item["properties"].get("url", []), item["properties"].get("uid", []))
        else:
            try:
                items.add(item["properties"].get("uid", [])[0])
            except IndexError:
                try:
                    items.add(item["properties"].get("url", [])[0])
                except IndexError:
                    pass
        if "h-entry" in item["type"]:
            items.update(mf2py.get_url(item["properties"].get("author", [])))
            items.update(mf2py.get_url(item["properties"].get("comment", [])))
            items.update(mf2py.get_url(item["properties"].get("like", [])))
            items.update(mf2py.get_url(item["properties"].get("repost", [])))
            items.update(mf2py.get_url(item["properties"].get("bookmark", [])))
            items.update(mf2py.get_url(item["properties"].get("mention", [])))
            items.update(mf2py.get_url(item["properties"].get("like-of", [])))
            items.update(mf2py.get_url(item["properties"].get("repost-of", [])))
            items.update(mf2py.get_url(item["properties"].get("bookmark-of", [])))
            items.update(mf2py.get_url(item["properties"].get("in-reply-to", [])))

            # TODO index h-entry specific data
        if "h-feed" in item["type"]:
            items.update(mf2py.get_url(item.get("children", [])))

            # TODO index feed data?
    print(f'Returning {len(items)} links scraped from {url}')
    return items.difference(crawled_pages)

async def worker(name: str, q: asyncio.Queue, http: aiohttp.ClientSession):
    while True:
        url = await q.get()
        try:
            new_urls = await crawl_page(name, http, url)
            for i in new_urls:
                await q.put(i)
        finally:
            q.task_done()
            await asyncio.sleep(2)


async def main():
    q = asyncio.Queue()
    http = aiohttp.ClientSession(headers={
        "User-Agent": "crawler.fireburn.ru/{__version__}"
    })
    try:
        print(f"Starting crawl from {sys.argv[1]}")
        await q.put(sys.argv[1])
        tasks = []
        for i in range(int(sys.argv[2])):
            print(f"Creating task {i}")
            task = worker(f'worker-{i}', q, http)
            tasks.append(task)

        print("Running...")
        await asyncio.gather(*tasks)
    finally:
        await http.close()
        print("\n\n\n")
        for i in list(crawled_pages):
            print(i)
